const sql = require("mssql");

const pools = {};

const config = {
  user: "", // insert own login
  password: "", // insert own login
  server: "70.188.110.8",
  database: "Southeastern",
  port: 14658,
  pool: {
    max: 10,
    min: 0,
    idleTimeoutMillis: 30000,
  },
};
// manage a set of pools by name (config will be required to create the pool)
// a pool will be removed when it is closed
async function getPool(name) {
  if (!Object.prototype.hasOwnProperty.call(pools, name)) {
    const pool = new sql.ConnectionPool(config);
    const close = pool.close.bind(pool);
    pool.close = (...args) => {
      delete pools[name];
      return close(...args);
    };
    await pool.connect();
    pools[name] = pool;
  }
  return pools[name];
}

// close all pools
function closeAll() {
  return Promise.all(
    Object.values(pools).map((pool) => {
      return pool.close();
    })
  );
}

module.exports = {
  closeAll,
  getPool,
};
