const express = require("express"),
  app = express(),
  port = process.env.PORT || 5000,
  cors = require("cors");
const { getPool } = require("./database");
const bodyParser = require("body-parser");
const util = require("util");
const { v4: uuidv4 } = require("uuid");

app.use(cors());
app.listen(port, () => console.log("Backend server live on " + port));
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

app.use(bodyParser.json());

function getDate() {
  let date_ob = new Date();
  let date = ("0" + date_ob.getDate()).slice(-2);
  // current month
  let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
  // current year
  let year = date_ob.getFullYear();
  // current hours
  let hours = date_ob.getHours();
  // current minutes
  let minutes = date_ob.getMinutes();
  // current seconds
  let seconds = date_ob.getSeconds();
  let currentdate =
    year + "-" + month + "-" + date + " " + hours + ":" + minutes;
  return currentdate;
}
async function runQuery(query) {
  // pool will always be connected when the promise has resolved - may reject if the connection config is invalid
  const pool = await getPool("default");
  const result = await pool.request().query(query);
  var values = result;
  // console.log(values);
  return values;
}

app.get("/", (req, res) => {
  res.send({ message: "We did it!" });
});

app.get("/tickets", async (req, res) => {
  const result = await runQuery("Select * from Southeastern.DT_Tickets;");
  res.status(200).send(result);
});
app.get("/ER", async (req, res) => {
  const result = await runQuery(
    "Select * from Southeastern.DT_Engagement_Rounds;"
  );
  res.status(200).send(result.recordset);
});
app.get("/HS", async (req, res) => {
  const result = await runQuery(
    "Select * from Southeastern.DT_HealthAndSafety;"
  );
  res.status(200).send(result);
});
app.get("/HS/:id", async (req, res) => {
  id = req.params.id;
  query = util.format(
    "Select * from Southeastern.DT_HealthAndSafety where hsid='%s'",
    id
  );
  const result = await runQuery(query);
  res.status(200).send(result.recordset);
});
app.post("/tickets/insert", async (req, res) => {
  newuuid = uuidv4();
  userquery = util.format(
    "Select clientID from Southeastern.DT_Users where fname='%s' and lname='%s';",
    req.body.fname,
    req.body.lname
  );
  const user = await runQuery(userquery);
  const userid = user.recordset[0].clientID;
  let currentdate = getDate();

  query = util.format(
    "Insert into Southeastern.DT_Tickets (ticket_id, user_id, building, room, problem, category,image,priority, datetime) values ('%s','%s','%s','%s','%s','%s','%s','%s','%s')",
    newuuid,
    userid,
    req.body.building,
    req.body.room,
    req.body.problem,
    req.body.category,
    req.body.image,
    1,
    currentdate
  );

  const result = await runQuery(query);
  res.status(201).send();
});
app.post("/ER/insert", async (req, res) => {
  //! Round1
  // console.log(req.body);
  round1Query = util.format(
    "Insert into Southeastern.DT_ERounds(Round_Time, Comments) Output Inserted.Round_Id values ('%s','%s')",
    req.body.Round_Time1,
    req.body.R1Comments
  );
  results = await runQuery(round1Query);
  Round1_ID = results.recordset[0].Round_Id;
  //! Round2
  round2Query = util.format(
    "Insert into Southeastern.DT_ERounds(Round_Time, Comments) Output Inserted.Round_Id values ('%s','%s')",
    req.body.Round_Time2,
    req.body.R2Comments
  );
  results = await runQuery(round2Query);
  Round2_ID = results.recordset[0].Round_Id;
  //! Round3
  round3Query = util.format(
    "Insert into Southeastern.DT_ERounds(Round_Time, Comments) Output Inserted.Round_Id values ('%s','%s')",
    req.body.Round_Time3,
    req.body.R3Comments
  );
  results = await runQuery(round3Query);
  Round3_ID = results.recordset[0].Round_Id;
  //?EAUX
  EauxQuery = util.format(
    "Insert into Southeastern.DT_Eaux(signs, doors, extlights, intlights, elevator,trash,parlights) Output Inserted.eaux_id values ('%s','%s','%s','%s','%s','%s','%s')",
    req.body.signs,
    req.body.doors,
    req.body.extlights,
    req.body.intlights,
    req.body.elevator,
    req.body.trash,
    req.body.parlights
  );
  results = await runQuery(EauxQuery);
  eaux_id = results.recordset[0].eaux_id;

  //! Engagement Round
  userquery = util.format(
    "Select clientID from Southeastern.DT_Users where fname='%s' and lname='%s';",
    req.body.fname,
    req.body.lname
  );
  const user = await runQuery(userquery);
  const userid = user.recordset[0].clientID;
  engagementQuery = util.format(
    "Insert into Southeastern.DT_Engagement_Rounds(clientID, date, building, round1, round2, round3, eaux) values ('%s','%s','%s','%s','%s','%s','%s')",
    userid,
    req.body.date,
    req.body.Building,
    Round1_ID,
    Round2_ID,
    Round3_ID,
    eaux_id
  );
  const ER = await runQuery(engagementQuery);

  res.status(201).send(ER);
});
app.post("/HS", async (req, res) => {
  console.log(req.body);
  userquery1 = util.format(
    "Select clientID from Southeastern.DT_Users where fname='%s' and lname='%s';",
    req.body.ra1fname,
    req.body.ra1lname
  );
  user = await runQuery(userquery1);
  const ra1 = user.recordset[0].clientID;
  userquery2 = util.format(
    "Select clientID from Southeastern.DT_Users where fname='%s' and lname='%s';",
    req.body.ra2fname,
    req.body.ra2lname
  );
  user = await runQuery(userquery2);
  const ra2 = user.recordset[0].clientID;

  insertQuery = util.format(
    "Insert Into Southeastern.DT_HealthAndSafety(hsid,ra1_clientID, ra2_clientID, building, floor, date) Output Inserted.hsid values ('%s','%s','%s','%s','%s','%s')",
    uuidv4(),
    ra1,
    ra2,
    req.body.building,
    req.body.floor,
    getDate()
  );

  const hs = await runQuery(insertQuery);
  res.status(201).send(hs.recordset[0]);
});
app.post("/HSI", async (req, res) => {
  console.log(req.body);
  insertQuery = util.format(
    "Insert Into Southeastern.DT_HealthAndSafetyIssues(id, hsid,room, category, problem, datetime, priority_level) Output Inserted.hsid values ('%s','%s','%s','%s','%s','%s','%s')",
    uuidv4(),
    req.body.hsid,
    req.body.room,
    req.body.category,
    req.body.problem,
    getDate(),
    "1"
  );

  const hsi = await runQuery(insertQuery);

  res.status(201).send(hsi.recordset[0]);
});
app.post('/custom', async (req, res) => {
  var result = await runQuery(req.body.query)
  res.send(result)
})