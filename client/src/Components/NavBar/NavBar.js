import React from "react";
import {
  Navbar,
  Nav,
  NavDropdown,
  Form,
  Button,
  FormControl,
} from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { Link } from "react-router-dom";

function NavBar() {
  return (
    <Navbar bg="light" expand="lg">
      <Navbar.Brand href="/">RA Portal</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <NavDropdown title="View" id="basic-nav-dropdown">
            <NavDropdown.Item href="/viewTickets">Ticket</NavDropdown.Item>
            <NavDropdown.Item href="/viewHealthAndSafety">
              Health and Safety
            </NavDropdown.Item>
            <NavDropdown.Item href="/viewEngagementRound">
              Engagement Round
            </NavDropdown.Item>
          </NavDropdown>
          <NavDropdown title="Edit" id="basic-nav-dropdown">
            <NavDropdown.Item href="/editTickets">Ticket</NavDropdown.Item>
            <NavDropdown.Item href="/editHealthAndSafety">
              Health and Safety
            </NavDropdown.Item>
            <NavDropdown.Item href="/editEngagementRound">
              Engagement Round
            </NavDropdown.Item>
          </NavDropdown>
          <Nav.Link href="/customQuery">Custom Query</Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}

export default NavBar;
