import React, {Component} from 'react';
import axios from 'axios';

class ViewEngagementRounds extends Component{

    state={
        rounds:[]
    }

    divStyles={
        paddingLeft: '20px'
    }


    getAllRoundsRoute = 'http://localhost:5000/ER'

    componentDidMount() {
        axios.get(this.getAllRoundsRoute)
            .then((response) =>{
                var rounds = response.data
                this.setState({rounds})
                for(var i=0; i<rounds.length; i++){
                    console.log(rounds[i])
                }
            })
            .catch(
                (error) =>{
                    console.log(error)
                }
            )
    }

    render() {
        return (
            <div style={this.divStyles}>
                {this.state.rounds.map(round =>
                    <li>
                        Date: {round.date} <br/> <br/>
                        Building Number: {round.building} <br/>
                        Round 1: {round.round1} <br/>
                        Round 2: {round.round2} <br/>
                        Round 3: {round.round3} <br/> <br/>
                    </li>
                )}

            </div>
        );
    }
}

export default ViewEngagementRounds;