import React, { Component, useState } from "react";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import { Dropdown, DropdownButton } from "react-bootstrap";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import axios from "axios";

function EditEngagementRounds() {
  const useStyles = makeStyles((theme) => ({
    container: {
      display: "flex",
      flexWrap: "wrap",
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: 200,
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
  }));

  const classes = useStyles();

  const addEngagementRoundRoute = "http://localhost:5000/ER/insert";

  const [selectedDate1, handleDateChange1] = useState("2020-01-01");
  const [selectedTime1, handleTimeChange1] = useState("");
  const [selectedTime2, handleTimeChange2] = useState("");
  const [selectedTime3, handleTimeChange3] = useState("");
  const [buildingNumber, handleBuildingNumber] = useState(0);

  const [signs, setSigns] = useState(0);
  const [doors, setDoors] = useState(0);
  const [extLights, setExtLights] = useState(0);
  const [elevator, setElevator] = useState(0);
  const [trash, setTrash] = useState(0);
  const [parLights, setParLights] = useState(0);
  const [intLights, setIntLights] = React.useState(0);
  const [open1, setOpen1] = React.useState(false);
  const [open2, setOpen2] = React.useState(false);
  const [open3, setOpen3] = React.useState(false);
  const [open4, setOpen4] = React.useState(false);
  const [open6, setOpen6] = React.useState(false);
  const [open7, setOpen7] = React.useState(false);
  const [open8, setOpen8] = React.useState(false);
  const [r1Comments, setR1Comments] = useState("");
  const [r2Comments, setR2Comments] = useState("");
  const [r3Comments, setR3Comments] = useState("");
  const [fname, setfname] = useState("");
  const [lname, setlname] = useState("");

  const handleFnameChange = (event) => {
    setfname(event.target.value);
  };

  const handleLnameChange = (event) => {
    setlname(event.target.value);
  };

  const handleIntLightsChange = (event) => {
    setIntLights(event.target.value);
  };

  const handleExtLightsChange = (event) => {
    setExtLights(event.target.value);
  };

  const handleSignsChange = (event) => {
    setSigns(event.target.value);
  };

  const handleDoorsChange = (event) => {
    setDoors(event.target.value);
  };

  const handleElevatorChange = (event) => {
    setElevator(event.target.value);
  };

  const handleTrashChange = (event) => {
    setTrash(event.target.value);
  };

  const handleParLightsChange = (event) => {
    setParLights(event.target.value);
  };

  const handleR1CommentsChange = (event) => {
    setR1Comments(event.target.value);
  };

  const handleR2CommentsChange = (event) => {
    setR2Comments(event.target.value);
  };

  const handleR3CommentsChange = (event) => {
    setR3Comments(event.target.value);
  };

  const handleClose1 = () => {
    setOpen1(false);
  };

  const handleOpen1 = () => {
    setOpen1(true);
  };

  const handleClose2 = () => {
    setOpen2(false);
  };

  const handleOpen2 = () => {
    setOpen2(true);
  };

  const handleClose3 = () => {
    setOpen3(false);
  };

  const handleOpen3 = () => {
    setOpen3(true);
  };

  const handleClose4 = () => {
    setOpen4(false);
  };

  const handleOpen4 = () => {
    setOpen4(true);
  };

  const handleClose6 = () => {
    setOpen6(false);
  };

  const handleOpen6 = () => {
    setOpen6(true);
  };

  const handleClose7 = () => {
    setOpen7(false);
  };

  const handleOpen7 = () => {
    setOpen7(true);
  };

  const handleClose8 = () => {
    setOpen8(false);
  };

  const handleOpen8 = () => {
    setOpen8(true);
  };

  function TimePickers1() {
    const classes = useStyles();

    return (
      <form className={classes.container} noValidate>
        <TextField
          id="time"
          type="time"
          value={selectedTime1}
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
          onChange={(val) => {
            handleTimeChange1(val.currentTarget.value);
            console.log(val.currentTarget.value);
          }}
        />
      </form>
    );
  }

  function TimePickers2() {
    const classes = useStyles();

    return (
      <form className={classes.container} noValidate>
        <TextField
          id="time"
          type="time"
          value={selectedTime2}
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
          onChange={(val) => {
            handleTimeChange2(val.currentTarget.value);
            console.log(val.currentTarget.value);
          }}
        />
      </form>
    );
  }

  function TimePickers3() {
    const classes = useStyles();

    return (
      <form className={classes.container} noValidate>
        <TextField
          id="time"
          type="time"
          value={selectedTime3}
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          inputProps={{
            step: 300, // 5 min
          }}
          onChange={(val) => {
            handleTimeChange3(val.currentTarget.value);
            console.log(val.currentTarget.value);
          }}
        />
      </form>
    );
  }

  function DatePickers1() {
    const classes = useStyles();

    return (
      <form className={classes.container} noValidate>
        <TextField
          id="date"
          type="date"
          value={selectedDate1}
          defaultValue="2020-05-24"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          onChange={(val) => {
            handleDateChange1(val.currentTarget.value);
            console.log(val.currentTarget.value);
          }}
        />
      </form>
    );
  }

  let divStyle = {
    paddingLeft: "40px",
    paddingBottom: "20px",
    paddingTop: "20px",
  };

  let engagementRound = {
    Round_Time1: selectedTime1,
    R1Comments: r1Comments,
    Round_Time2: selectedTime2,
    R2Comments: r2Comments,
    Round_Time3: selectedTime3,
    R3Comments: r3Comments,

    signs: signs,
    doors: doors,
    extlights: extLights,
    intlights: intLights,
    elevator: elevator,
    trash: trash,
    parlights: parLights,

    fname: fname,
    lname: lname,
    date: selectedDate1, // 2012-03-12
    Building: buildingNumber,
  };

  let handleClick = async () => {
    console.log(engagementRound);
    await axios
      .post(addEngagementRoundRoute, engagementRound)
      .then(console.log("Ticket created"))
      .catch((e) =>
        console.log("An error has occurred. Ticket was not issued.")
      );
  };

  return (
    <div style={divStyle}>
      <form noValidate autoComplete="off">
        <TextField
          id="standard-basic"
          label="First Name"
          onChange={(val) => {
            handleFnameChange(val);
          }}
        />{" "}
        <br />
        <TextField
          id="standard-basic"
          label="Last Name"
          onChange={(val) => {
            handleLnameChange(val);
          }}
        />
      </form>
      <br /> <br />
      <p>Buliding Number</p>
      <DropdownButton id="dropdown-basic-button" title={buildingNumber}>
        <Dropdown.Item
          value={"Louisiana"}
          onClick={() => handleBuildingNumber(1)}
        >
          Louisiana
        </Dropdown.Item>
        <Dropdown.Item
          value={"Twelve Oaks"}
          onClick={() => handleBuildingNumber(2)}
        >
          Twelve Oaks
        </Dropdown.Item>
        <Dropdown.Item
          value={"Ascension"}
          onClick={() => handleBuildingNumber(3)}
        >
          Ascension
        </Dropdown.Item>
        <Dropdown.Item value={"Pride"} onClick={() => handleBuildingNumber(4)}>
          Pride
        </Dropdown.Item>
        <Dropdown.Item
          value={"Hammond"}
          onClick={() => handleBuildingNumber(5)}
        >
          Hammond
        </Dropdown.Item>
        <Dropdown.Item value={"Taylor"} onClick={() => handleBuildingNumber(7)}>
          Taylor
        </Dropdown.Item>
        <Dropdown.Item
          value={"Tangipahoa"}
          onClick={() => handleBuildingNumber(8)}
        >
          Tangipahoa
        </Dropdown.Item>
        <Dropdown.Item
          value={"Cardinal Newman"}
          onClick={() => handleBuildingNumber(9)}
        >
          Cardinal Newman
        </Dropdown.Item>
        <Dropdown.Item
          value={"Livingston"}
          onClick={() => handleBuildingNumber(10)}
        >
          Livingston
        </Dropdown.Item>
        <Dropdown.Item
          value={"St. Tammany"}
          onClick={() => handleBuildingNumber(11)}
        >
          St. Tammany
        </Dropdown.Item>
        <Dropdown.Item
          value={"Washington"}
          onClick={() => handleBuildingNumber(12)}
        >
          Washingotn
        </Dropdown.Item>
        <Dropdown.Item
          value={"Southeastern Oaks Building 3"}
          onClick={() => handleBuildingNumber(13)}
        >
          Southeastern Oaks Building 3
        </Dropdown.Item>
        <Dropdown.Item
          value={"Southeastern Oaks Building 1"}
          onClick={() => handleBuildingNumber(14)}
        >
          Southeastern Oaks Building 1
        </Dropdown.Item>
        <Dropdown.Item
          value={"Southeastern Oaks Building 2"}
          onClick={() => handleBuildingNumber(15)}
        >
          Southeastern Oaks Building 2
        </Dropdown.Item>
        <Dropdown.Item
          value={"Southeastern Oaks Building 4"}
          onClick={() => handleBuildingNumber(16)}
        >
          Southeastern Oaks Building 4
        </Dropdown.Item>
        <Dropdown.Item
          value={"Southeastern Oaks Building 5"}
          onClick={() => handleBuildingNumber(17)}
        >
          Southeastern Oaks Building 5
        </Dropdown.Item>
        <Dropdown.Item
          value={"Southeastern Oaks Building 6A"}
          onClick={() => handleBuildingNumber(18)}
        >
          Southeastern Oaks Building 6A
        </Dropdown.Item>
        <Dropdown.Item
          value={"Southeastern Oaks Building 6B"}
          onClick={() => handleBuildingNumber(19)}
        >
          Southeastern Oaks Building 6B
        </Dropdown.Item>
        <Dropdown.Item
          value={"Village M"}
          onClick={() => handleBuildingNumber(20)}
        >
          Village M
        </Dropdown.Item>
        <Dropdown.Item
          value={"Greek Commons"}
          onClick={() => handleBuildingNumber(21)}
        >
          Greek Commons
        </Dropdown.Item>
        <Dropdown.Item
          value={"Alpha Sigma Tau"}
          onClick={() => handleBuildingNumber(22)}
        >
          Alpha Sigma Tau
        </Dropdown.Item>
        <Dropdown.Item
          value={"Village B"}
          onClick={() => handleBuildingNumber(23)}
        >
          Village B
        </Dropdown.Item>
        <Dropdown.Item
          value={"Village C"}
          onClick={() => handleBuildingNumber(24)}
        >
          Village C
        </Dropdown.Item>
        <Dropdown.Item
          value={"Phi Mu"}
          onClick={() => handleBuildingNumber(25)}
        >
          Phi Mu
        </Dropdown.Item>
        <Dropdown.Item
          value={"Sigma Sigma Sigma"}
          onClick={() => handleBuildingNumber(26)}
        >
          Sigma Sigma Sigma
        </Dropdown.Item>
        <Dropdown.Item
          value={"Theta Phi Alpha"}
          onClick={() => handleBuildingNumber(27)}
        >
          Theta Phi Alpha
        </Dropdown.Item>
        <Dropdown.Item
          value={"Alpha Omicron Pi"}
          onClick={() => handleBuildingNumber(28)}
        >
          Alpha Omicron Pi
        </Dropdown.Item>
        <Dropdown.Item
          value={"Pi Kappa Alpha"}
          onClick={() => handleBuildingNumber(29)}
        >
          Pi Kappa Alpha
        </Dropdown.Item>
        <Dropdown.Item
          value={"Sigma Tau Gamma"}
          onClick={() => handleBuildingNumber(30)}
        >
          Sigma Tau Gamma
        </Dropdown.Item>
        <Dropdown.Item
          value={"Delta Tau Delta"}
          onClick={() => handleBuildingNumber(31)}
        >
          Delta Tau Delta
        </Dropdown.Item>
      </DropdownButton>{" "}
      <br /> <br />
      <p>Date</p>
      <DatePickers1 />
      <br /> <br />
      <p>Round 1 Time</p>
      <TimePickers1 />
      <br />
      <TextField
        id="standard-basic"
        label="Round 1 Comments"
        onChange={(val) => {
          handleR1CommentsChange(val);
        }}
      />
      <br /> <br /> <br />
      <p>Round 2 Time</p>
      <TimePickers2 />
      <br />
      <TextField
        id="standard-basic"
        label="Round 2 Comments"
        onChange={(val) => {
          handleR2CommentsChange(val);
        }}
      />
      <br /> <br /> <br />
      <p>Round 3 TIme</p>
      <TimePickers3 />
      <br />
      <TextField
        id="standard-basic"
        label="Round 3 Comments"
        onChange={(val) => {
          handleR3CommentsChange(val);
        }}
      />
      <br /> <br /> <br />
      <div>
        <p>Issues</p>
        <FormControl className={classes.formControl}>
          <InputLabel id="demo-controlled-open-select-label">
            Interior Lights
          </InputLabel>
          <Select
            labelId="demo-controlled-open-select-label"
            id="demo-controlled-open-select"
            open={open1}
            onClose={handleClose1}
            onOpen={handleOpen1}
            value={intLights}
            onChange={handleIntLightsChange}
          >
            <MenuItem value={0}>
              <em>None</em>
            </MenuItem>
            <MenuItem value={1}>Yes</MenuItem>
            <MenuItem value={0}>No</MenuItem>
          </Select>
        </FormControl>
        <br />

        <FormControl className={classes.formControl}>
          <InputLabel id="demo-controlled-open-select-label">
            Exterior Lights
          </InputLabel>
          <Select
            labelId="demo-controlled-open-select-label"
            id="demo-controlled-open-select"
            open={open2}
            onClose={handleClose2}
            onOpen={handleOpen2}
            value={extLights}
            onChange={handleExtLightsChange}
          >
            <MenuItem value={0}>
              <em>None</em>
            </MenuItem>
            <MenuItem value={1}>Yes</MenuItem>
            <MenuItem value={0}>No</MenuItem>
          </Select>
        </FormControl>
        <br />

        <FormControl className={classes.formControl}>
          <InputLabel id="demo-controlled-open-select-label">Signs</InputLabel>
          <Select
            labelId="demo-controlled-open-select-label"
            id="demo-controlled-open-select"
            open={open3}
            onClose={handleClose3}
            onOpen={handleOpen3}
            value={signs}
            onChange={handleSignsChange}
          >
            <MenuItem value={0}>
              <em>None</em>
            </MenuItem>
            <MenuItem value={1}>Yes</MenuItem>
            <MenuItem value={0}>No</MenuItem>
          </Select>
        </FormControl>
        <br />

        <FormControl className={classes.formControl}>
          <InputLabel id="demo-controlled-open-select-label">Doors</InputLabel>
          <Select
            labelId="demo-controlled-open-select-label"
            id="demo-controlled-open-select"
            open={open4}
            onClose={handleClose4}
            onOpen={handleOpen4}
            value={doors}
            onChange={handleDoorsChange}
          >
            <MenuItem value={0}>
              <em>None</em>
            </MenuItem>
            <MenuItem value={1}>Yes</MenuItem>
            <MenuItem value={0}>No</MenuItem>
          </Select>
        </FormControl>
        <br />

        <FormControl className={classes.formControl}>
          <InputLabel id="demo-controlled-open-select-label">
            Elevator
          </InputLabel>
          <Select
            labelId="demo-controlled-open-select-label"
            id="demo-controlled-open-select"
            open={open6}
            onClose={handleClose6}
            onOpen={handleOpen6}
            value={elevator}
            onChange={handleElevatorChange}
          >
            <MenuItem value={0}>
              <em>None</em>
            </MenuItem>
            <MenuItem value={1}>Yes</MenuItem>
            <MenuItem value={0}>No</MenuItem>
          </Select>
        </FormControl>
        <br />

        <FormControl className={classes.formControl}>
          <InputLabel id="demo-controlled-open-select-label">Trash</InputLabel>
          <Select
            labelId="demo-controlled-open-select-label"
            id="demo-controlled-open-select"
            open={open7}
            onClose={handleClose7}
            onOpen={handleOpen7}
            value={trash}
            onChange={handleTrashChange}
          >
            <MenuItem value={0}>
              <em>None</em>
            </MenuItem>
            <MenuItem value={1}>Yes</MenuItem>
            <MenuItem value={0}>No</MenuItem>
          </Select>
        </FormControl>
        <br />

        <FormControl className={classes.formControl}>
          <InputLabel id="demo-controlled-open-select-label">
            Parking Lights
          </InputLabel>
          <Select
            labelId="demo-controlled-open-select-label"
            id="demo-controlled-open-select"
            open={open8}
            onClose={handleClose8}
            onOpen={handleOpen8}
            value={parLights}
            onChange={handleParLightsChange}
          >
            <MenuItem value={0}>
              <em>None</em>
            </MenuItem>
            <MenuItem value={1}>Yes</MenuItem>
            <MenuItem value={0}>No</MenuItem>
          </Select>
        </FormControl>
        <br />
      </div>
      <Button onClick={handleClick}> hi</Button>
    </div>
  );
}

export default EditEngagementRounds;
