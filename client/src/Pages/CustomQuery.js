import React, { Component } from "react";
import { Button, Container, Form, Table } from "react-bootstrap";
import axios from "axios";

import "bootstrap/dist/css/bootstrap.min.css";

class CustomQuery extends Component {
  state = {
    query: "",
    data: [],
    keyset: [],
  };

  queryRoute = "http://localhost:5000/custom";

  queryhandler = (event) => {
    this.setState({ query: event.target.value });
  };

  handleSubmit = async (event) => {
    event.preventDefault();
    await axios
      .post(this.queryRoute, this.state)
      .then((result) => {
        var data = result.data.recordset;
        var keyset = Object.keys(data[0]);
        this.setState({ data });
        this.setState({ keyset });
        console.log(this.state);
      })
      .catch((e) => console.log("An error has occurred."));
  };

  render() {
    return (
      <Container>
        <Form>
          <Form.Group>
            <Form.Label>Enter Query to run:</Form.Label>
            <Form.Control onChange={this.queryhandler}></Form.Control>
          </Form.Group>
          <Button variant="primary" onClick={this.handleSubmit}>
            Submit
          </Button>
        </Form>
        <Table striped bordered hover>
          <thead>
            <tr>
              {this.state.keyset.map((k) => (
                <th>{k}</th>
              ))}
              {/* <th>Ticket ID</th>
              <th>Category</th>
              <th>Building</th>
              <th>Room number</th>
              <th>Priority</th>
              <th>Status</th> */}
            </tr>
          </thead>
          <tbody>
            {this.state.data.map((d) => (
              <tr>
                {this.state.keyset.map((e) => (
                  <td>{d[e]}</td>
                ))}
              </tr>
            ))}
          </tbody>
        </Table>
      </Container>
    );
  }
}

export default CustomQuery;
