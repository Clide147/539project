import React, { Component } from "react";
import axios from "axios";
import { Table, Container } from "react-bootstrap";
class ViewTickets extends Component {
  state = {
    tickets: [],
  };

  getAllTicketsRoute = "http://localhost:5000/tickets";

  componentDidMount() {
    axios
      .get(this.getAllTicketsRoute)
      .then((response) => {
        var tickets = response.data.recordset;
        this.setState({ tickets });
        for (var i = 0; i < tickets.length; i++) {
          console.log(tickets[i]);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    return (
      <div>
        <Container>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Ticket ID</th>
                <th>Category</th>
                <th>Building</th>
                <th>Room number</th>
                <th>Priority</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              {this.state.tickets.map((ticket) => (
                <tr>
                  <td>{ticket.ticket_id} </td>
                  <td>{ticket.category} </td>
                  <td>{ticket.building} </td>
                  <td>{ticket.room} </td>
                  <td>{ticket.priority} </td>
                  <td>{ticket.status} </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Container>
      </div>
    );
  }
}

export default ViewTickets;
