import React, { Component } from "react";
import {
  Form,
  Button,
  Dropdown,
  DropdownButton,
  Container,
  Row,
  Col,
} from "react-bootstrap";
import axios from "axios";

import "bootstrap/dist/css/bootstrap.min.css";

class EditTickets extends Component {
  state = {
    fname: "",
    lname: "",
    building: "",
    room: 0,
    problem: "",
    category: "Please select...",
    image: null,
  };

  formStyle = {
    paddingLeft: "40px",
    paddingBottom: "20px",
  };

  createTicketsRoute = "http://localhost:5000/tickets/insert";

  firstNameChangeHandler = (event) => {
    this.setState({ fname: event.target.value });
  };

  lastNameChangeHandler = (event) => {
    this.setState({ lname: event.target.value });
  };

  buildingChangeHandler = (event) => {
    this.setState({ building: event.target.value });
  };

  roomChangeHandler = (event) => {
    this.setState({ room: event.target.value });
  };

  problemChangeHandler = (event) => {
    this.setState({ problem: event.target.value });
  };

  categoryChangeHandler = (event) => {
    this.setState({ category: event.target.value });
  };

  handleSubmit = async (event) => {
    event.preventDefault();
    await axios
      .post(this.createTicketsRoute, this.state)
      .then(console.log(this.state))
      .catch((e) =>
        console.log("An error has occurred. Ticket was not issued.")
      );
  };

  render() {
    return (
      <Container>
        <Row className="justify-content-md-center">
          <Col xs lg="6">
            <Form style={this.formStyle}>
              <h1>Create a Ticket</h1>
              <Form.Group>
                <Form.Label>First Name</Form.Label>
                <Form.Control
                  type={"text"}
                  onChange={this.firstNameChangeHandler}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label>Last Name</Form.Label>
                <Form.Control
                  type={"text"}
                  onChange={this.lastNameChangeHandler}
                />
              </Form.Group>
              <Form.Group>
                {" "}
                <Form.Label>Buliding Number</Form.Label>
                <Form.Control onChange={this.buildingChangeHandler} as="select">
                  <option disabled selected value>
                    {" "}
                    -- select an option --{" "}
                  </option>{" "}
                  <option value="1">Louisiana</option>
                  <option value="2">Twelve Oaks</option>
                  <option value="3">Ascension</option>
                  <option value="4">Pride</option>
                  <option value="5">Hammond</option>
                  <option value="7">Taylor</option>
                  <option value="8">Tangipahoa</option>
                  <option value="9">Cardinal Newman</option>
                  <option value="10">Livingston</option>
                  <option value="11">St. Tammany</option>
                  <option value="12">Washingotn</option>
                  <option value="13">Southeastern Oaks Building 3</option>
                  <option value="14">Southeastern Oaks Building 1</option>
                  <option value="15">Southeastern Oaks Building 2</option>
                  <option value="16">Southeastern Oaks Building 4</option>
                  <option value="17">Southeastern Oaks Building 5</option>
                  <option value="18">Southeastern Oaks Building 6A</option>
                  <option value="19">Southeastern Oaks Building 6B</option>
                  <option value="20">Village M</option>
                  <option value="21">Greek Commons</option>
                  <option value="22">Alpha Sigma Tau</option>
                  <option value="23">Village B</option>
                  <option value="24">Village C</option>
                  <option value="25">Phi Mu</option>
                  <option value="26">Sigma Sigma Sigma</option>
                  <option value="27">Theta Phi Alpha</option>
                  <option value="28">Alpha Omicron Pi</option>
                  <option value="29">Pi Kappa Alpha</option>
                  <option value="30">Sigma Tau Gamma</option>
                  <option value="31">Delta Tau Delta</option>
                </Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label>Room Number</Form.Label>
                <Form.Control type={"text"} onChange={this.roomChangeHandler} />
              </Form.Group>
              <Form.Group>
                <Form.Label>Description of Problem</Form.Label>
                <Form.Control
                  type={"text"}
                  onChange={this.problemChangeHandler}
                />{" "}
              </Form.Group>
              <Form.Group>
                <Form.Label>Category of your issue</Form.Label>
                <Form.Control
                  id="dropdown-basic-button"
                  title={this.state.category}
                  as="select"
                  onChange={(this, this.categoryChangeHandler)}
                >
                  <option disabled selected value>
                    {" "}
                    -- select an option --{" "}
                  </option>
                  <option value={"Pest"}>Pest</option>
                  <option value={"Mold"}>Mold</option>
                  <option value={"Maintenance"}>Maintenance</option>
                  <option value={"Internet"}>Internet</option>
                  <option value={"Cable"}>Cable</option>
                </Form.Control>
              </Form.Group>
              <Form.Group>
                <Button variant="primary" onClick={this.handleSubmit}>
                  Submit
                </Button>
              </Form.Group>
            </Form>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default EditTickets;
