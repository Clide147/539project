import React, { Component } from "react";
import axios from "axios";
import {
  Button,
  Form,
  Container,
  Row,
  Col,
  FormControl,
} from "react-bootstrap";

class EditHealthAndSafety extends Component {
  state = {
    main: {
      ra1fname: "",
      ra1lname: "",
      ra2fname: "",
      ra2lname: "",
      building: "",
      floor: "",
    },
    issue: {
      hsid: "",
      room: 0,
      category: "",
      problem: "",
    },
  };
  formStyle = {
    paddingLeft: "40px",
    paddingBottom: "20px",
  };

  submitMainHS = "http://localhost:5000/HS";
  submitHSI = "http://localhost:5000/HSI";

  componentDidMount() {
    axios
      .get(this.getAllHealthSafety)
      .then((response) => {
        var healthsafety = response.data.recordset;
        this.setState({ healthsafety });
        for (var i = 0; i < healthsafety.length; i++) {
          console.log(healthsafety[i]);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }
  ra1fnamehandler = (event) => {
    var main = { ...this.state.main };
    main.ra1fname = event.target.value;
    this.setState({ main });
  };
  ra1lnamehandler = (event) => {
    var main = { ...this.state.main };
    main.ra1lname = event.target.value;
    this.setState({ main });
  };
  ra2fnamehandler = (event) => {
    var main = { ...this.state.main };
    main.ra2fname = event.target.value;
    this.setState({ main });
  };
  ra2lnamehandler = (event) => {
    var main = { ...this.state.main };
    main.ra2lname = event.target.value;
    this.setState({ main });
  };
  buildinghandler = (event) => {
    var main = { ...this.state.main };
    main.building = event.target.value;
    this.setState({ main });
  };
  floorhandler = (event) => {
    var main = { ...this.state.main };
    main.floor = event.target.value;
    this.setState({ main });
  };

  roomhandler = (event) => {
    var issue = { ...this.state.issue };
    issue.room = event.target.value;
    this.setState({ issue });
  };
  categoryhandler = (event) => {
    var issue = { ...this.state.issue };
    issue.category = event.target.value;
    this.setState({ issue });
  };
  problemhandler = (event) => {
    var issue = { ...this.state.issue };
    issue.problem = event.target.value;
    this.setState({ issue });
  };
  submitMain = async (event) => {
    event.preventDefault();
    await axios.post(this.submitMainHS, this.state.main).then((response) => {
      var issue = { ...this.state.issue };
      issue.hsid = response.data.hsid;
      this.setState({ issue });
    });
  };
  submitIssue = async (event) => {
    event.preventDefault();
    await axios.post(this.submitHSI, this.state.issue).then((response) => {
      //   var issue = { ...this.state.issue };
      //   issue.hsid = response.data.hsid;
      //   issue.category = "";
      //   issue.problem = "";
      //   issue.room = "";
      //   this.setState({ issue });
      console.log(response);
    });
  };
  render() {
    return (
      <div>
        <Container id="main">
          <h1>Create Health and Safety</h1>
          <Row className="justify-content-md-center">
            <Col xs lg="6">
              <Form>
                <Form.Group controlId="formBasicEmail">
                  <Form.Label>First Reisdent Assistant</Form.Label>
                  <Row>
                    <Col>
                      <Form.Control
                        placeholder="First name"
                        onChange={this.ra1fnamehandler}
                      />
                    </Col>
                    <Col>
                      <Form.Control
                        placeholder="Last name"
                        onChange={this.ra1lnamehandler}
                      />
                    </Col>
                  </Row>
                </Form.Group>
                <Form.Group controlId="formBasicEmail">
                  <Form.Label>Second Reisdent Assistant</Form.Label>
                  <Row>
                    <Col>
                      <Form.Control
                        placeholder="First name"
                        onChange={this.ra2fnamehandler}
                      />
                    </Col>
                    <Col>
                      <Form.Control
                        placeholder="Last name"
                        onChange={this.ra2lnamehandler}
                      />
                    </Col>
                  </Row>
                </Form.Group>
                <Form.Group controlId="formBasicEmail">
                  <Form.Label>Building</Form.Label>
                  <Form.Control
                    onChange={this.buildinghandler}
                    as="select"
                    size="sm"
                    custom
                  >
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </Form.Control>
                </Form.Group>
                <Form.Group controlId="formBasicEmail">
                  <Form.Label>Floor</Form.Label>
                  <Form.Control
                    onChange={this.floorhandler}
                    as="select"
                    size="sm"
                    custom
                  >
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </Form.Control>
                </Form.Group>
                <Button variant="primary" onClick={this.submitMain}>
                  Start
                </Button>
              </Form>
            </Col>
          </Row>
        </Container>
        <Container id="issues">
          <h1>Create Health and Safety Issue</h1>
          <Row className="justify-content-md-center">
            <Col xs lg="6">
              <Form>
                <Form.Group controlId="formBasicEmail">
                  <Row>
                    <Col>
                      <Form.Label>Room Number</Form.Label>
                      <Form.Control
                        onChange={this.roomhandler}
                        placeholder="Room number"
                      />
                    </Col>
                    <Col>
                      {" "}
                      <Form.Label>Category</Form.Label>
                      <Form.Control
                        onChange={this.categoryhandler}
                        placeholder="Category"
                      />
                    </Col>
                  </Row>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Issue</Form.Label>

                  <FormControl as="textarea" onChange={this.problemhandler} />
                </Form.Group>
                <Button variant="primary" onClick={this.submitIssue}>
                  Submit
                </Button>
              </Form>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default EditHealthAndSafety;
