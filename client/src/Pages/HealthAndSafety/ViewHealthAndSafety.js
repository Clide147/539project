import React, { Component } from "react";
import axios from "axios";
import { Table, Container } from "react-bootstrap";

class ViewhealthAndSafety extends Component {
  state = {
    healthsafety: [],
  };

  getAllHealthSafety = "http://localhost:5000/HS";

  componentDidMount() {
    axios
      .get(this.getAllHealthSafety)
      .then((response) => {
        var healthsafety = response.data.recordset;
        this.setState({ healthsafety });
        for (var i = 0; i < healthsafety.length; i++) {
          console.log(healthsafety[i]);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    return (
      <div>
        <Container>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Health and Safety ID</th>
                <th>Building</th>
                <th>Floor</th>
                <th>Date and Time</th>
                <th>Resident Assistant 1</th>
                <th>Resident Assistant 2</th>
              </tr>
            </thead>
            <tbody>
              {this.state.healthsafety.map((healthsafety) => (
                <tr>
                  <td>{healthsafety.hsid} </td>
                  <td>{healthsafety.building}</td>
                  <td>{healthsafety.floor}</td>
                  <td>{healthsafety.date}</td>
                  <td>{healthsafety.ra1_clientID}</td>
                  <td>{healthsafety.ra2_clientID} </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Container>
      </div>
    );
  }
}

export default ViewhealthAndSafety;
