import React, { useState } from "react";
import "./App.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Axios from "axios";
import NavBar from "./Components/NavBar/NavBar";
import Home from './Pages/Home/Home'
import EditEngagementRound from './Pages/EngagementRound/EditEngagementRound';
import ViewEngagmentRound from './Pages/EngagementRound/ViewEngagementRound';
import EditHealthAndSafety from './Pages/HealthAndSafety/EditHealthAndSafety';
import ViewHealthAndSafety from './Pages/HealthAndSafety/ViewHealthAndSafety';
import EditTickets from './Pages/Tickets/EditTickets';
import ViewTickets from './Pages/Tickets/ViewTickets'
import CustomQuery from "./Pages/CustomQuery";

function App() {
  return (

    <BrowserRouter>
      <div>
        <NavBar />
        <Switch>
          <Route exact path={"/"} component={Home} />
          <Route exact path={"/editEngagementRound"} component={EditEngagementRound} />
          <Route exact path={"/viewEngagementRound"} component={ViewEngagmentRound} />
          <Route exact path={"/editHealthAndSafety"} component={EditHealthAndSafety} />
          <Route exact path={"/viewHealthAndSafety"} component={ViewHealthAndSafety} />
          <Route exact path={"/editTickets"} component={EditTickets} />
          <Route exact path={"/viewTickets"} component={ViewTickets} />
          <Route exact path={"/customQuery"} component={CustomQuery} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
